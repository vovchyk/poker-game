package com.company.pokergame.services;

import com.company.pokergame.models.Amount;
import com.company.pokergame.util.InvalidMoveException;

public interface Dealer {

    boolean canCheck(Player player);

    boolean canCall(Player player);

    void call(Player player) throws InterruptedException, InvalidMoveException;

    void check(Player player) throws InterruptedException, InvalidMoveException;

    void raise(Player player, Amount amount) throws InterruptedException, InvalidMoveException;

    void allIn(Player player) throws InterruptedException, InvalidMoveException;

    void fold(Player player) throws InterruptedException, InvalidMoveException;

}
