package com.company.pokergame.services;

import com.company.pokergame.models.Card;

import java.util.Collection;
import java.util.UUID;

public interface Player extends Comparable<Player> {

    UUID getId();

    String getName();

    void makeMove(GameBoard board, Dealer dealer, Collection<Card> playerCards) throws InterruptedException;

}
