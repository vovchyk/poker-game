package com.company.pokergame.services.impl;

import com.company.pokergame.models.Card;
import com.company.pokergame.services.GameBoard;
import com.company.pokergame.services.Dealer;
import com.company.pokergame.util.InvalidMoveException;

import java.util.Collection;

public class Bot extends AbstractPlayer {

    public Bot(String name) {
        super(name);
    }

    @Override
    public void makeMove(GameBoard board, Dealer dealer, Collection<Card> playerCards) throws InterruptedException {
        try {
            // NOTE: thinking...
            Thread.sleep(System.currentTimeMillis() % 2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            if (dealer.canCheck(this)) {
                dealer.check(this);
            } else if (dealer.canCall(this)) {
                dealer.call(this);
            } else if (System.currentTimeMillis() % 2 == 0) {
                dealer.allIn(this);
            } else {
                dealer.fold(this);
            }
        } catch (InvalidMoveException e) {
            throw new RuntimeException(e);
        }
    }
}
