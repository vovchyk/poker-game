package com.company.pokergame.services.impl;

import com.company.pokergame.models.Amount;
import com.company.pokergame.models.Card;
import com.company.pokergame.services.GameBoard;
import com.company.pokergame.services.Dealer;
import com.company.pokergame.util.InvalidMoveException;

import java.util.Arrays;
import java.util.Collection;

public class User extends AbstractPlayer {

    public interface MovesReader {
        String readMove();
    }

    private final MovesReader mMovesReader;

    public User(String name, MovesReader movesReader) {
        super(name);

        mMovesReader = movesReader;
    }

    @Override
    public void makeMove(GameBoard board, Dealer dealer, Collection<Card> playerCards) throws InterruptedException {
        while (true) {
            System.out.println("\n\nYour cards: " + Arrays.toString(playerCards.toArray()));
            System.out.print("Your move: ");
            String move = mMovesReader.readMove();

            try {
                switch (move) {
                    case "call":
                        dealer.call(this);
                        return;

                    case "check":
                        dealer.check(this);
                        return;

                    case "fold":
                        dealer.fold(this);
                        return;

                    case "allIn":
                        dealer.allIn(this);
                        return;

                    case "exit":
                        throw new InterruptedException();

                    default:
                        if (move.startsWith("raise:")) {
                            try {
                                double amount = Double.parseDouble(move.substring("raise:".length()));
                                dealer.raise(this, new Amount(amount));
                                return;
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }
                        System.out.println("Unknown move. Please try again.");
                }
            } catch (InvalidMoveException e) {
                System.out.println("Invalid move. Please try again.");
            }
        }
    }
}
