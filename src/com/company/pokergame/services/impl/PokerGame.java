package com.company.pokergame.services.impl;

import com.company.pokergame.models.Amount;
import com.company.pokergame.models.Card;
import com.company.pokergame.util.InvalidMoveException;
import com.company.pokergame.util.PlayerInfo;
import com.company.pokergame.services.Player;
import com.company.pokergame.services.GameBoard;
import com.company.pokergame.services.Dealer;

import java.util.*;

public class PokerGame implements GameBoard, Dealer {

    public static final double SMALL_BLIND = 100;

    private final LinkedList<Card> mDeck;

    private final ArrayList<Card> mShownCards = new ArrayList<>(5);

    private int mRound = 1;

    private Amount mLastBet;

    private Amount mBudget;

    private final HashMap<Player, PlayerInfo> mPlayerInfoMap = new HashMap<>();

    private PlayerInfo mActivePlayer;
    private PlayerInfo mRaisePlayer;

    public PokerGame(List<PlayerInfo> players) {
        if (players.size() < 2) {
            throw new IllegalStateException();
        }

        PlayerInfo prevPlayer = players.get(0);
        mActivePlayer = prevPlayer;

        mPlayerInfoMap.put(mActivePlayer.getPlayer(), mActivePlayer);

        for (int i = 1; i < players.size(); i++) {
            PlayerInfo playerInfo = players.get(i);

            mPlayerInfoMap.put(playerInfo.getPlayer(), playerInfo);

            prevPlayer.setNextPlayer(playerInfo);
            playerInfo.setPrevPlayer(prevPlayer);
            prevPlayer = playerInfo;
        }

        prevPlayer.setNextPlayer(mActivePlayer);
        mActivePlayer.setPrevPlayer(prevPlayer);

        mActivePlayer.setState(PlayerInfo.State.SmallBlind);
        mActivePlayer.getNextPlayer().setState(PlayerInfo.State.BigBlind);

        mRaisePlayer = mActivePlayer;

        mLastBet = new Amount(SMALL_BLIND);

        mBudget = new Amount(0);

        mDeck = new LinkedList<>();

        Card.Suits[] suits = Card.Suits.values();

        for (Card.Suits suit : suits) {
            for (int i = 0; i < Card.RANK_NUM; i++) {
                mDeck.add(new Card(suit, i));
            }
        }
    }

    @Override
    public int getRound() {
        return mRound;
    }

    @Override
    public boolean canCheck(Player player) {
        return mRound > 1 && mLastBet.isZero();
    }

    @Override
    public boolean canCall(Player player) {
        return !mLastBet.isZero() && getBalance(player).getValue() >= mLastBet.getValue();
    }

    @Override
    public Amount getBalance(Player player) {
        return mPlayerInfoMap.get(player).getBalance();
    }

    @Override
    public Collection<Card> getShownCards() {
        return mShownCards;
    }

    @Override
    public Amount getLastBet() {
        return mLastBet;
    }

    public void shuffle() {
        final Random rand = new Random(System.currentTimeMillis());
        final ArrayList<Card> cards = new ArrayList<>(mDeck);
        final int len = cards.size();
        final int swapNum = 100 * len; // NOTE: just in case J

        for (int i = 0; i < swapNum; i++) {
            final int card1Index = Math.abs(rand.nextInt()) % len;
            final int card2Index = Math.abs(rand.nextInt()) % len;
            if (card1Index != card2Index) {
                Card tmp = cards.get(card1Index);
                cards.set(card1Index, cards.get(card2Index));
                cards.set(card2Index, tmp);
            }
        }

        mDeck.clear();
        mDeck.addAll(cards);
    }

    public void deal() {
        PlayerInfo playerInfo = mActivePlayer;

        do {
            ArrayList<Card> cards = new ArrayList<>(2);
            cards.add(mDeck.pop());
            cards.add(mDeck.pop());

            playerInfo.deal(cards);

            playerInfo = playerInfo.getNextPlayer();
        } while (playerInfo != mActivePlayer);
    }

    public void start() throws InterruptedException {
        mActivePlayer.getPlayer().makeMove(this, this, mActivePlayer.getCards());
    }

    private void finish(PlayerInfo winner) {
        winner.setBalance(winner.getBalance().add(mBudget));
        mBudget = new Amount(0);

        System.out.println("\n\n" + winner.toString() + " won."
                + "\nResults: " + Arrays.toString(mPlayerInfoMap.values().toArray())
                + "\nCards on the table: " + Arrays.toString(mShownCards.toArray()));
    }

    @Override
    public void call(Player player) throws InterruptedException, InvalidMoveException {
        if (mPlayerInfoMap.get(player) != mActivePlayer) {
            throw new IllegalArgumentException();
        }

        if (mRound == 1
                && mActivePlayer.getState() != PlayerInfo.State.SmallBlind
                && mLastBet.getValue() < 2 * SMALL_BLIND) {
            mLastBet = new Amount(2 * SMALL_BLIND);
        }

        if (mLastBet.isZero()) {
            throw new InvalidMoveException();
        }

        System.out.println(String.format("Player[%s]: call %s", mActivePlayer.getPlayer().getName(), mLastBet.toString()));

        mActivePlayer.setBalance(mActivePlayer.getBalance().deduct(mLastBet));
        mBudget = mBudget.add(mLastBet);

        proceed();
    }

    @Override
    public void check(Player player) throws InterruptedException, InvalidMoveException {
        if (mPlayerInfoMap.get(player) != mActivePlayer) {
            throw new IllegalArgumentException();
        }

        if (mRound > 1) {
            System.out.println(String.format("Player[%s]: check", mActivePlayer.getPlayer().getName()));

            proceed();
        } else {
            throw new InvalidMoveException();
        }
    }

    @Override
    public void raise(Player player, Amount amount) throws InterruptedException, InvalidMoveException {
        if (amount.getValue() <= mLastBet.getValue()) {
            throw new InvalidMoveException();
        }

        if (mPlayerInfoMap.get(player) != mActivePlayer) {
            throw new IllegalArgumentException();
        }

        if (mActivePlayer.getBalance().getValue() < amount.getValue()
                || amount.getValue() <= mLastBet.getValue()) {
            throw new InvalidMoveException();
        }

        System.out.println(String.format("Player[%s]: raise %s", mActivePlayer.getPlayer().getName(), amount.toString()));

        mActivePlayer.setBalance(mActivePlayer.getBalance().deduct(amount));
        mBudget = mBudget.add(amount);
        mLastBet = amount;
        mRaisePlayer = mActivePlayer;

        proceed();
    }

    @Override
    public void allIn(Player player) throws InterruptedException, InvalidMoveException {
        if (mPlayerInfoMap.get(player) != mActivePlayer || mActivePlayer.allIn()) {
            throw new IllegalArgumentException();
        }

        System.out.println(String.format("Player[%s]: allIn", mActivePlayer.getPlayer().getName()));

        Amount amount = mActivePlayer.getBalance();
        mActivePlayer.setBalance(Amount.ZERO);
        mBudget = mBudget.add(amount);
        if (amount.getValue() > mLastBet.getValue()) {
            mLastBet = amount;
            mRaisePlayer = mActivePlayer;
        }

        proceed();
    }

    @Override
    public void fold(Player player) throws InterruptedException, InvalidMoveException {
        if (mPlayerInfoMap.get(player) != mActivePlayer) {
            throw new IllegalArgumentException();
        }

        if (mBudget.getValue() < 3 * SMALL_BLIND) {
            throw new InvalidMoveException();
        }

        System.out.println(String.format("Player[%s]: fold", mActivePlayer.getPlayer().getName()));

        PlayerInfo prevPlayer = mActivePlayer.getPrevPlayer();
        PlayerInfo nextPlayer = mActivePlayer.getNextPlayer();

        prevPlayer.setNextPlayer(nextPlayer);
        nextPlayer.setPrevPlayer(prevPlayer);

        if (prevPlayer == nextPlayer) {
            finish(prevPlayer);
        } else {
            mActivePlayer = prevPlayer;
            proceed();
        }
    }

    private void proceed() throws InterruptedException, InvalidMoveException {
        System.out.println("\nCurrent results: " + Arrays.toString(mPlayerInfoMap.values().toArray())
                + "\nCards on the table: " + Arrays.toString(mShownCards.toArray()));

        PlayerInfo activePlayer = mActivePlayer.getNextActivePlayer();
        if (activePlayer == null) {
            checkWinner();
        } else {
            mActivePlayer = activePlayer;
            if (mActivePlayer == mRaisePlayer) {
                if (mRound == 5) {
                    checkWinner();
                    return;
                } else {
                    mShownCards.add(mDeck.pop());
                    mRound++;
                    mLastBet = Amount.ZERO;
                }
            }

            mActivePlayer.getPlayer().makeMove(this, this, mActivePlayer.getCards());
        }
    }

    private void checkWinner() {
        PlayerInfo winner = null;
        int winnerRank = 0;

        PlayerInfo playerInfo = mActivePlayer;
        do {
            int playerBiggestRank = getBiggestRank(playerInfo);
            if (playerBiggestRank > winnerRank) {
                winner = playerInfo;
                winnerRank = playerBiggestRank;
            }

            playerInfo = playerInfo.getNextPlayer();
        } while (playerInfo != mActivePlayer);

        if (winner == null) {
            throw new IllegalStateException();
        }

        finish(winner);
    }

    private static int getBiggestRank(PlayerInfo playerInfo) {
        int max = 0;

        for (Card card : playerInfo.getCards()) {
            if (card.getRank() > max) {
                max = card.getRank();
            }
        }

        return max;
    }
}
