package com.company.pokergame.services.impl;

import com.company.pokergame.services.Player;

import java.util.UUID;

public abstract class AbstractPlayer implements Player {

    private final UUID mId = UUID.randomUUID();

    private final String mName;

    AbstractPlayer(String name) {
        mName = name;
    }

    @Override
    public UUID getId() {
        return mId;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public int compareTo(Player o) {
        return mId.compareTo(o.getId());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Player) {
            return mId.equals(((Player) obj).getId());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }
}
