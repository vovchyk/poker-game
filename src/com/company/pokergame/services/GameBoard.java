package com.company.pokergame.services;

import com.company.pokergame.models.Amount;
import com.company.pokergame.models.Card;

import java.util.Collection;

public interface GameBoard {

    int getRound();

    Collection<Card> getShownCards();

    Amount getBalance(Player player);

    Amount getLastBet();

}
