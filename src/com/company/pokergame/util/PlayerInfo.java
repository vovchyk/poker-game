package com.company.pokergame.util;

import com.company.pokergame.models.Amount;
import com.company.pokergame.models.Card;
import com.company.pokergame.services.Player;

import java.util.Collection;
import java.util.Collections;

public class PlayerInfo {

    public enum State {
        Default, SmallBlind, BigBlind
    }

    private final Player mPlayer;

    private Amount mBalance;

    private Collection<Card> mCards;

    private PlayerInfo mPrevPlayer;
    private PlayerInfo mNextPlayer;

    private State mState = State.Default;

    public PlayerInfo(Player player, Amount balance) {
        mPlayer = player;
        mBalance = balance;
    }

    public Player getPlayer() {
        return mPlayer;
    }

    public PlayerInfo getPrevPlayer() {
        return mPrevPlayer;
    }

    public void setPrevPlayer(PlayerInfo mPrevPlayer) {
        this.mPrevPlayer = mPrevPlayer;
    }

    public PlayerInfo getNextPlayer() {
        return mNextPlayer;
    }

    public void setNextPlayer(PlayerInfo mNextPlayer) {
        this.mNextPlayer = mNextPlayer;
    }

    public PlayerInfo getNextActivePlayer() {
        PlayerInfo nextPlayerInfo = mNextPlayer;

        while (nextPlayerInfo.allIn()) {
            nextPlayerInfo = nextPlayerInfo.mNextPlayer;

            if (nextPlayerInfo == this) {
                return null;
            }
        }

        return nextPlayerInfo;
    }

    public boolean allIn() {
        return getBalance().getValue() == 0;
    }

    public Amount getBalance() {
        return mBalance;
    }

    public void setBalance(Amount mBalance) {
        this.mBalance = mBalance;
    }

    public Collection<Card> getCards() {
        return mCards;
    }

    public State getState() {
        return mState;
    }

    public void setState(State mState) {
        this.mState = mState;
    }

    public void deal(Collection<Card> cards) {
        mCards = cards;
    }

    public Collection<Card> reset() {
        final Collection<Card> cards = mCards;

        mCards = Collections.emptyList();

        return cards;
    }

    @Override
    public String toString() {
        return String.format("{ %s : %f %s }", mPlayer.getName(), mBalance.getValue(), mBalance.getCurrency());
    }
}
