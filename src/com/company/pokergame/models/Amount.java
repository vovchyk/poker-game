package com.company.pokergame.models;

public class Amount {

    public static final Amount ZERO = new Amount(0);

    private final String mCurrency = "USD"; //TODO: Hardcoded for now

    // TODO: for simplicity `double` type has been used. In real app we will need some `money` type.
    private final double mValue;

    public Amount(double value) {
        if (value < 0) {
            throw new IllegalArgumentException();
        }

        mValue = value;
    }

    public boolean isZero() {
        return mValue == ZERO.getValue();
    }

    public String getCurrency() {
        return mCurrency;
    }

    public double getValue() {
        return mValue;
    }

    public Amount deduct(double amount) {
        if (amount > mValue) {
            throw new IllegalArgumentException();
        }

        return new Amount(mValue - amount);
    }

    public Amount deduct(Amount amount) {
        if (amount.getValue() > mValue || !amount.getCurrency().equals(mCurrency)) {
            throw new IllegalArgumentException();
        }

        return new Amount(mValue - amount.getValue());
    }

    public Amount add(double amount) {
        return new Amount(mValue + amount);
    }

    public Amount add(Amount amount) {
        if (!amount.getCurrency().equals(mCurrency)) {
            throw new IllegalArgumentException();
        }

        return new Amount(mValue + amount.getValue());
    }

    @Override
    public String toString() {
        return String.format("%f %s", mValue, mCurrency);
    }
}
