package com.company.pokergame.models;

public class Card {

    public enum Suits {
        HEARTS, SPADES, DIAMONDS, CLUBS
    }

    public static final int RANK_NUM = 13;

    private final Suits mSuit;

    private final int mRank;

    public Card(Suits suit, int rank) {
        if (suit == null || rank < 0 || rank >= RANK_NUM) {
            throw new IllegalArgumentException();
        }

        mSuit = suit;
        mRank = rank;
    }

    public Suits getSuit() {
        return mSuit;
    }

    public int getRank() {
        return mRank;
    }

    public String getDisplayName() {
        switch (mSuit) {
            case CLUBS:
                return getName(mRank,"♣");
            case DIAMONDS:
                return getName(mRank,"♦");
            case HEARTS:
                return getName(mRank,"♥");
            case SPADES:
                return getName(mRank,"♠");
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    public String toString() {
        return getDisplayName();
    }

    private static String getName(int rank, String suitDisplayName) {
        String rankName;

        if (rank == 0) {
            rankName = "Ace";
        } else if (rank < 10) {
            rankName = "" + (rank + 1);
        } else {
            switch (rank) {
                case 10:
                    rankName = "Jack";
                    break;
                case 11:
                    rankName = "Queen";
                    break;
                case 12:
                    rankName = "King";
                    break;
                default:
                    throw new IllegalStateException();
            }
        }

        return rankName + " " + suitDisplayName;
    }
}
