package com.company.pokergame;

import com.company.pokergame.models.Amount;
import com.company.pokergame.util.PlayerInfo;
import com.company.pokergame.services.impl.Bot;
import com.company.pokergame.services.impl.PokerGame;
import com.company.pokergame.services.impl.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        ArrayList<PlayerInfo> playerInfoList = new ArrayList<>();
        playerInfoList.add(new PlayerInfo(new Bot("Bot #1"), new Amount(10_000)));
        playerInfoList.add(new PlayerInfo(new Bot("Bot #2"), new Amount(10_000)));

        final Scanner scanner = new Scanner(System.in);
        final User.MovesReader movesReader = scanner::nextLine;
        playerInfoList.add(new PlayerInfo(new User("Me", movesReader), new Amount(10_000)));

        List<PlayerInfo> players = playerInfoList;
        do {
            PokerGame game = new PokerGame(players);

            game.shuffle();

            game.deal();

            try {
                System.out.print("\nNew Game Started!\n\n");
                game.start();
            } catch (InterruptedException e) {
                break;
            }

            players = playerInfoList.stream().filter(pi -> pi.getBalance().getValue() >= PokerGame.SMALL_BLIND)
                    .collect(Collectors.toList());
        } while (players.size() > 1);

        System.out.print("\nGame finished with results: " + Arrays.toString(players.toArray()));
    }
}
